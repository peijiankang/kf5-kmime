%global framework kmime

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:           kf5-%{framework}
Version:        23.08.5
Release:        1
Summary:        The KMime Library

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %majmin_ver_kf5
%global stable %stable_kf5
Source0:        http://download.kde.org/%{stable}/release-service/%{version}/src/%{framework}-%{version}.tar.xz

BuildRequires:  boost-devel
BuildRequires:  extra-cmake-modules
BuildRequires:  kf5-rpm-macros
BuildRequires:  qt5-qtbase-devel
BuildRequires:  kf5-ki18n-devel >= 5.15
BuildRequires:  kf5-kcodecs-devel >= 5.15
%if 0%{?tests}
BuildRequires:  dbus-x11
BuildRequires:  xorg-x11-server-Xvfb
%endif

Conflicts:      kde-l10n < 17.03

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       boost-devel
Requires:       kf5-kdelibs4support-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{version} -p1


%build
mkdir %{_target_platform}
pushd %{_target_platform}
%{cmake_kf5} .. \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
popd
%make_build  -C %{_target_platform}


%install
make install/fast DESTDIR=%{buildroot}  -C %{_target_platform}

%find_lang %{name} --all-name --with-html


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif


%ldconfig_scriptlets

%files -f %{name}.lang
%license LICENSES/*
%{_kf5_datadir}/qlogging-categories5/%{framework}.*
%{_kf5_libdir}/libKPim5Mime.so.*

%files devel
%{_includedir}/KPim5/KMime/
%{_kf5_libdir}/libKPim5Mime.so
%{_kf5_libdir}/cmake/KF5Mime/
%{_kf5_libdir}/cmake/KPim5Mime/
%{_kf5_archdatadir}/mkspecs/modules/qt_KMime.pri


%changelog
* Fri Mar 15 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.5-1
- update verison to 23.08.5

* Mon Jan 08 2024 peijiankang <peijiankang@kylinos.cn> - 23.08.4-1
- update verison to 23.08.4

* Wed Aug 09 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 23.04.3-1
- Update package to version 23.04.3

* Mon Feb 27 2023 peijiankang <peijiankang@kylinos.cn> - 22.12.0-1
- update verison to 22.12.0

* Mon Nov 21 2022 misaka00251 <liuxin@iscas.ac.cn> - 22.08.0-1
- Init package (Derived from federa project, thanks to fedora team)
